# Use an official Ubuntu base image
FROM ubuntu:22.04

# Avoid warnings by switching to noninteractive for the build process
ENV DEBIAN_FRONTEND=noninteractive
ENV USER=root
ENV BROWSER=/usr/local/bin/firefox

# Install basic utilities, CA certificates, and bzip2
RUN apt-get update && apt-get install -y --no-install-recommends \
    ca-certificates \
    wget \
    gpg \
    bzip2 \
    curl && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install XFCE, VNC server, dbus-x11 and xfonts-base
RUN apt-get update && apt-get install -y --no-install-recommends \
    dialog \
    xvfb \
    xfce4 \
    xfce4-goodies \
    tightvncserver \
    dbus-x11 \
    xfonts-base \
    libegl1-mesa-dev \
    libprotobuf-dev \
    liblua5.3 \
    libcrypto++ \
    libxkbcommon-x11-0 \
    libxcb-icccm4 \
    libxcb-keysyms1 \
    libxcb-xinerama0 \
    libxcb-cursor-dev \
    libasound2 \
    xdg-utils \
    libnss3 && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install Firefox without using Snap
RUN wget -O /tmp/firefox.tar.bz2 "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=en-US" --no-check-certificate && \
    tar -xjf /tmp/firefox.tar.bz2 -C /opt && \
    ln -s /opt/firefox/firefox /usr/local/bin/firefox && \
    rm /tmp/firefox.tar.bz2

# Add HoG repository and install HoG
RUN wget -q -O - https://archive.tibiahogbot.com/hogbot.key | gpg --yes --dearmor -o /usr/share/keyrings/hogbot.pgp && \
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/hogbot.pgp] https://archive.tibiahogbot.com private main" | tee -a /etc/apt/sources.list && \
    apt-get update && apt-get install -y --no-install-recommends hogbot && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Download and install Tibia
RUN wget https://static.tibia.com/download/tibia.x64.tar.gz && \
    tar -xzf tibia.x64.tar.gz -C /opt && \
    rm tibia.x64.tar.gz

# Install Tibia maps
RUN curl -o /tmp/install-tibia-maps https://raw.githubusercontent.com/tibiamaps/tibia-maps-installer-linux/main/install-tibia-maps && \
    chmod +x /tmp/install-tibia-maps && \
    /tmp/install-tibia-maps && \
    rm /tmp/install-tibia-maps

# Setup VNC server with password from build argument
ARG VNC_PASSWORD
RUN mkdir /root/.vnc && \
    echo "${VNC_PASSWORD:-password}" | vncpasswd -f > /root/.vnc/passwd && \
    chmod 600 /root/.vnc/passwd && \
    touch /root/.Xauthority

# Set display resolution (change as needed)
ENV RESOLUTION=1920x1080

# Expose VNC port
EXPOSE 5901

# Install ProtonVPN
RUN wget -q -O /tmp/protonvpn-stable-release.deb https://repo.protonvpn.com/debian/dists/stable/main/binary-all/protonvpn-stable-release_1.0.3-3_all.deb && \
    apt-get update && apt-get install -y gnupg && \
    dpkg -i /tmp/protonvpn-stable-release.deb && \
    apt-get update && \
    apt-get install -y protonvpn-cli && \
    rm /tmp/protonvpn-stable-release.deb && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set the working directory in the container
WORKDIR /app

# Copy the start script to the appropriate location and set permissions
COPY start-vnc.sh /usr/local/bin/start-vnc.sh
RUN chmod +x /usr/local/bin/start-vnc.sh

# Set the entrypoint to start the VNC server
ENTRYPOINT ["/usr/local/bin/start-vnc.sh"]
