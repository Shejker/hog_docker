### Docker Image Documentation

#### Overview
This Dockerfile sets up a Docker image based on Ubuntu 22.04 with the XFCE desktop environment, Firefox browser, Tibia game client, HoG (Hand of God) bot, and ProtonVPN. It enables VNC server access to interact with the XFCE desktop remotely.

#### Components Installed
- **Base Image**: Ubuntu 22.04
- **Desktop Environment**: XFCE4 and associated goodies
- **VNC Server**: TightVNCServer for remote desktop access
- **Browser**: Firefox installed from Mozilla binaries
- **Additional Packages**: Various dependencies for graphical applications and utilities
- **HoG Bot**: Installed from a custom repository
- **Tibia Game Client**: Installed from the official Tibia website
- **Tibia Maps**: Automatically installed using a script
- **ProtonVPN**: Installed using the official ProtonVPN repository
- **Resolution**: Default display resolution set to 1920x1080

#### Exposed Ports
- **5901**: VNC server port for remote desktop access

#### Build Arguments
- **VNC_PASSWORD**: Allows setting a custom password for VNC server during build time.

#### Start Script (`start-vnc.sh`)
This script manages the startup of the VNC server and XFCE desktop environment. Ensure it is correctly configured and executable as per your requirements.

#### Directory Structure
- **/app**: Working directory inside the container where the entry point script resides.
- **/usr/local/bin/start-vnc.sh**: Start script for VNC server and XFCE setup.

### Readme

#### How to Use This Docker Image

This Docker image provides a complete environment for running graphical applications with remote desktop access via VNC. Follow these steps to use the image effectively:

#### Building the Docker Image

1. **Clone the Repository** (if you haven't already):
   ```bash
   git clone https://gitlab.com/Shejker/hog_docker.git
   cd hog_docker
   ```

2. **Build the Docker Image**:
   ```bash
   docker build --build-arg VNC_PASSWORD=mypassword -t vnc-hog-image .
   ```

   Replace `mypassword` with your desired VNC password.

#### Running the Docker Container

3. **Start the Docker Container**:
   ```bash
   docker run -dt --rm --name vnc-hog-container -p 5901:5901 vnc-hog-image
   ```

#### Accessing the XFCE Desktop

4. **Connect to the VNC Server**:
   - Use a VNC viewer to connect to `localhost:5901` (or the appropriate IP if running remotely).
   - Enter the VNC password (`mypassword` in the example).

#### Using ProtonVPN

5. **Connecting to ProtonVPN**:
   - After accessing the container, you can connect to ProtonVPN using the `protonvpn-cli` command. For example:
     ```bash
     protonvpn-cli connect --fastest
     ```

#### Notes

- **Security**: Ensure the VNC password is strong and not hardcoded in your scripts or Dockerfile for production use.
- **Customization**: Modify the Dockerfile or scripts (`start-vnc.sh`) as needed to fit specific requirements or preferences.
- **Documentation**: Refer to the Dockerfile comments and this readme for further understanding and customization options.

This Docker image is ideal for development, testing, or educational purposes where graphical interfaces, VPN connectivity, and remote access are required within a Dockerized environment.